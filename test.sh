# The following environment variables must be set before running this script
# - AWS_ACCESS_KEY_ID
# - AWS_SECRET_ACCESS_KEY
# - AWS_DEFAULT_REGION (optional)

IMG_FILE="kitten.jpg"
APP_URL=$(terraform -chdir=tf output -raw application_url)
BUCKET_URL=$(terraform -chdir=tf output -raw bucket_url)

curl --location --request POST "$APP_URL" --form "s3Key=$IMG_FILE" --form "file=@$IMG_FILE"

echo
for suffix in '' '_200' '_75'
do
  echo "$BUCKET_URL/$IMG_FILE$suffix"
done
