terraform {
  cloud {
    organization = "mikakatua"

    workspaces {
      name = "sre-hiring-test"
    }
  }
}
