variable "aws_region" {
  description = "AWS region"
  type        = string
  default     = "eu-west-1"
}

variable "app_name" {
  description = "Application name"
  type        = string
  default     = "image-resizer"
}

variable "image_tag" {
  description = "Container image tag"
  default     = "latest"
}

variable "common_tags" {
  description = "Map of default tags"
  type        = map(any)
  default = {
    env     = "demo"
    project = "image resizer"
  }
}
