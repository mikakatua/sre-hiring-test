# DevOps technical solution
Deployment of the Node.js application on AWS using API Gateway and Lambda function

## Prerequisites
The following software requirements must be installed:
* Docker
* AWS CLI
* Terraform 

## Automated deployment with Terraform
Set the required environment variables with your AWS credentials:
* AWS_ACCESS_KEY_ID
* AWS_SECRET_ACCESS_KEY
* AWS_DEFAULT_REGION (optional)

Run the script to build and deploy the application:
```
bash deploy.sh
```
The script output prints the application URL

## Automated deployment with Gitlab
It is also possible to deploy the application using Gitlab CI. First, you will need an account on [Terraform Cloud](https://app.terraform.io/) to store the Terraform state. Then, modify the `tf/backend.tf` file with your organization and workspace names.

Also, you must create the following variables in your Gitlab repository:
* AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY with your AWS credentials
* TF_TOKEN with your Terraform Cloud API token

## Testing the application
Run the script to test the application:
```
bash test.sh
```
The script output prints the URLs of the images

## Cleaning up
To remove all the AWS resources, run:
```
bash cleanup.sh
```
