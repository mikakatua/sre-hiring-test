output "ecr_registry" {
  description = "ECR Repository URL"
  value       = aws_ecr_repository.ecr_repo.repository_url
}

output "bucket_url" {
  description = "S3 bucket URL"
  value       = "https://${aws_s3_bucket.image_bucket.bucket_domain_name}"
}

output "application_url" {
  description = "Image resize URL"
  value       = "${aws_apigatewayv2_api.image_api.api_endpoint}/image"
}
