
# #
# # # API GATEWAY
# #

resource "aws_apigatewayv2_api" "image_api" {
  name          = var.app_name
  protocol_type = "HTTP"
}

resource "aws_apigatewayv2_stage" "image_api" {
  api_id = aws_apigatewayv2_api.image_api.id

  name        = "$default"
  auto_deploy = true

  access_log_settings {
    destination_arn = aws_cloudwatch_log_group.image_api_logs.arn

    format = jsonencode({
      requestId               = "$context.requestId"
      sourceIp                = "$context.identity.sourceIp"
      requestTime             = "$context.requestTime"
      protocol                = "$context.protocol"
      httpMethod              = "$context.httpMethod"
      resourcePath            = "$context.resourcePath"
      routeKey                = "$context.routeKey"
      status                  = "$context.status"
      responseLength          = "$context.responseLength"
      integrationErrorMessage = "$context.integrationErrorMessage"
      }
    )
  }
}

resource "aws_apigatewayv2_integration" "image_api" {
  api_id = aws_apigatewayv2_api.image_api.id

  integration_uri        = aws_lambda_function.image_lambda.invoke_arn
  integration_type       = "AWS_PROXY"
  integration_method     = "POST"
  payload_format_version = "1.0"
}

resource "aws_apigatewayv2_route" "image_api" {
  api_id = aws_apigatewayv2_api.image_api.id

  route_key = "POST /image"
  target    = "integrations/${aws_apigatewayv2_integration.image_api.id}"
}

resource "aws_cloudwatch_log_group" "image_api_logs" {
  name = "/aws/api_gw/${aws_apigatewayv2_api.image_api.name}"

  retention_in_days = 30
}

resource "aws_lambda_permission" "image_api" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.image_lambda.function_name
  principal     = "apigateway.amazonaws.com"

  source_arn = "${aws_apigatewayv2_api.image_api.execution_arn}/*/*"
}


# #
# # # ECR REPOSITORY
# #

resource "aws_ecr_repository" "ecr_repo" {
  image_tag_mutability = "MUTABLE"
  name                 = var.app_name

  encryption_configuration {
    encryption_type = "AES256"
  }

  image_scanning_configuration {
    scan_on_push = false
  }
}


# #
# # # S3 BUCKET 
# # 

resource "aws_s3_bucket" "image_bucket" {
  bucket_prefix = var.app_name

  force_destroy = true
}


# #
# # # LAMBDA FUNCTION
# #

resource "aws_iam_role" "lambda_role" {
  assume_role_policy = jsonencode(
    {
      Statement = [
        {
          Action = "sts:AssumeRole"
          Effect = "Allow"
          Principal = {
            Service = "lambda.amazonaws.com"
          }
        },
      ]
      Version = "2012-10-17"
    }
  )
  description           = "Allows Lambda functions to call AWS services on your behalf."
  force_detach_policies = false
  managed_policy_arns = [
    "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole",
  ]
  max_session_duration = 3600
  name                 = "Lambda_S3_Policy"
  path                 = "/"

  inline_policy {
    name = "S3BucketAccess"
    policy = jsonencode(
      {
        Statement = [
          {
            Action = [
              "s3:*",
            ]
            Effect = "Allow"
            Resource = [
              aws_s3_bucket.image_bucket.arn,
              "${aws_s3_bucket.image_bucket.arn}/*"
            ]
          },
        ]
        Version = "2012-10-17"
      }
    )
  }
}

resource "aws_lambda_function" "image_lambda" {
  depends_on = [aws_ecr_repository.ecr_repo]

  function_name = var.app_name
  image_uri     = "${aws_ecr_repository.ecr_repo.repository_url}:${var.image_tag}"
  memory_size   = 128
  package_type  = "Image"
  role          = aws_iam_role.lambda_role.arn
  timeout       = 3

  environment {
    variables = {
      "S3_BUCKET" = aws_s3_bucket.image_bucket.id
    }
  }

  tracing_config {
    mode = "PassThrough"
  }
}

resource "aws_cloudwatch_log_group" "image_lambda_logs" {
  name = "/aws/lambda/${aws_lambda_function.image_lambda.function_name}"

  retention_in_days = 30
}
