# The following environment variables must be set before running this script
# - AWS_ACCESS_KEY_ID
# - AWS_SECRET_ACCESS_KEY
# - AWS_DEFAULT_REGION (optional)

# Terraform initialization
terraform -chdir=tf init

# Create a container repository in Amazon ECR
terraform -chdir=tf apply -target=aws_ecr_repository.ecr_repo -auto-approve

# Build and push the container image to the repository
REPO=$(terraform -chdir=tf output -raw ecr_registry)
REGION=$(echo $REPO | cut -f4 -d.)
TAG=latest

docker build -t $REPO:$TAG .

aws ecr get-login-password --region $REGION | docker login --username AWS --password-stdin $REPO

docker push $REPO:$TAG

# Deploy the AWS infrastruture and the Lambda function
terraform -chdir=tf apply -auto-approve

