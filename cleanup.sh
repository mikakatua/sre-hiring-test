# The following environment variables must be set before running this script
# - AWS_ACCESS_KEY_ID
# - AWS_SECRET_ACCESS_KEY
# - AWS_DEFAULT_REGION (optional)

# Delete all provisioned resources
terraform -chdir=tf destroy -auto-approve
